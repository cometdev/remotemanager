/*
 * myRemoteManager.h
 *
 *  Created on: 17 Jul 2018
 *      Author: mrutar
 */

#ifndef myRemoteManager_H_
#define myRemoteManager_H_

#include <Task.h>
#include <DocProperties.h>

typedef enum{
RM_STARTING,
RM_STARTED,
RM_ACTIVE,
RM_INACTIVE,
RM_UNKNOWN,
RM_STOPPING,
RM_STOPPED,
}STATUS;

class myRemoteManager: public Task {
private:
	static void _RemoteNotificator();
    static myRemoteManager* _rmInstance;
	static void* _onWifiStart(void* sender, void *args);
    static void* _onWifiConnected(void* sender, void *args);
    static void* _onWifiStop(void* sender, void *args);
    STATUS status;
    String password=DEFAULT_TV_PASSWORD;
//protected:
    public:
	// protected data members/methods to support task
    void startTeamViewer();
    void stopTeamViewer();
    void enableTeamViewer(int val);
    int32_t getTeamViewerStatus();
    void getTeamViewerInfo();
    int32_t setPassword(String newpassword);
//public:
	//static DocProperties* wifiproperties;
    myRemoteManager() { _id = TASK + 1; _rmInstance = this; _name = "myRemoteManager"; };
  //  myRemoteManager() { _id = myRemoteManager; _rmInstance = this; _name = "myRemoteManager"; };
    virtual ~myRemoteManager() { };
	Message* _start() override;
	int32_t _action(Message* msg = NULL) override;
	bool _notify(Message* msg = NULL) override;
	// static for interface with C-style code
    static myRemoteManager* GetInstance();
	static int32_t getValue_cb(void *value);
    int32_t getStatus() { return status;}
    void setStatus(STATUS val) { status = val;}
    //static int32_t onWifiConnected(void *value);
};

#endif /* myRemoteManager_H_ */
