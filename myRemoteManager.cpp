/*
 * myRemoteManager.cpp
 *
 *  Created on: 17 Jul 2018
 *      Author: mrutar
 */

#include "myRemoteManager.h"
#include "ConfigMngr.h"
#include "WifiManager.h"
#include "Response.h"
#include "cmdGetPropertyValue.h"
#include <QDebug>
#include <iostream>

myRemoteManager* myRemoteManager::_rmInstance = NULL;
#define MAX_CMD_STRLEN 100
#define CMD_OUTLINE_LEN 1024
#define MAX_ID_LEN 30
#define MAX_PASSWD_LEN 10
#define MIN_PASSWD_LEN 8
#define ID_TAG "TeamViewer ID:"
#define ACTIVE_STATUS_TAG "Active:"
#define ERROR_STATUS_TAG "n/a (error)"
#define STARTED_OP "systemctl start teamviewerd.service"
#define STOPPED_OP	"systemctl stop teamviewerd.service"
#define INVALID_PWD -1
#define STATUS_VALUE_INACTIVE "inactive (dead)"
#define STATUS_VALUE_ACTIVE "active (running)"
#define DEFAULT_PASSWORD "comet123"
#define SUCCESS_OUTPUT "ok"

bool wifi_autoRun = false;


int myRemoteManager::setPassword(String newPassword)
{
    int ret =0;
    bool  invalid = false;
    //There is no option to find teamviewer password backdoor,so when we start teamviewer we set password as "comet"
    //User has an option to change it to any valid one
    if((newPassword == NULL) || (newPassword.Size() == 0))
    {
        newPassword = password;
                qInfo()<<"Password is set to default "<< newPassword.ToString().c_str();
    }
    //check for invalid characters- assuming GUI keyboard takescare of special chars
    //make sure length of password is atleast 6 and not > 12 - confirm with Pete
    //teamviewer errors -
    //password too short - use at least 8 characters [E11](although it allows 6)
    //password too long  - use 12 or less characters [E12]
    //Error: no password specified
    if(newPassword.Size() > 12 || newPassword.Size() < 6)
    {
        //TODO let GUI know that its an invalid length password and give suitable watrning
        invalid = true;
    }
    if(invalid) return 1;
    password = newPassword;
    String changecmd = "teamviewer passwd ";
    changecmd +=password;
    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
            qInfo()<<"Password changing- "<< changecmd.ToString().c_str()<<endl;
    fp = popen(changecmd.Data(), "r"); // start app and open stream to do capture data incoming from Noddy
    if(fp == NULL)
    {
        qInfo()<<"\n app doesnt exist?"<<endl;
        ret = 1;
        //shouldnot happen but if happens- notify GUI with error message
    }
    while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
    {
        if(strstr(buffer,SUCCESS_OUTPUT))
        {
         qInfo()<<"Password changed successfully to "<< endl;
         setStatus(RM_STARTED);
        }
     }
     if(fp) pclose(fp);

     return ret;
}
/*inline std::string trim(std::string& str)
{
    str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
    str.erase(str.find_last_not_of(' ')+1);         //surfixing spaces
    return str;
}*/
void myRemoteManager::getTeamViewerInfo() {
    String infocmd = "teamviewer info";

    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
    char idStr[MAX_ID_LEN];
    char statusStr[CMD_OUTLINE_LEN];	;
    char idTag[MAX_ID_LEN];int id = 0;
    char temp[150];
    char temp2[150];
    int started = 0;
    qInfo()<<"in getTeamViewerInfo"<<getStatus()<<endl;
    if(getStatus() == RM_STARTING)
    {
            qInfo()<<"TV started already"<<endl;
        //std::array<char,CMD_OUTLINE_LEN> buffer;

        fp = popen(infocmd.Data(), "r");
        if(fp == NULL)
        {
            qInfo()<<"\n app doesnt exist?"<<endl;

        }
        while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
        {
            if(strstr(buffer,ID_TAG))
            {
                //TODO: get ride of ANSI colour code proper way
                sscanf(buffer, "%[^:]:%[^0-9]%*4c%[^\n]",temp,temp2,idStr);
                qDebug()<<"ID tag"<<temp<<endl;
                qDebug()<<"ID is"<<idStr<<endl;
               // sscanf(buffer, "%*[^:0-9]%[^\n]",temp,id);
            }
            else if(strstr(buffer,ACTIVE_STATUS_TAG))
            {
                sscanf(buffer, "%[^\t\n:]:%[^\t\n]",&idTag,&statusStr);
            }
        }
        if(fp) pclose(fp);
        if(idStr != NULL)
        {
            //idStr = idStr.Trim();
            qInfo()<<"\n ID :"<<idStr<<endl;
            if(statusStr != NULL) qInfo()<<"\n Status: "<<statusStr<<endl;
            setStatus(RM_STARTED);
        }

     }
     if(getStatus() == RM_STARTED)
     {
         if(setPassword(NULL))
         {
             qInfo()<<"Problem in setting password"<<endl;
             setStatus(RM_STOPPING);
             stopTeamViewer();
         }
         else
         {
             qInfo()<<"Inform GUI >???not required here"<<endl;
             //All set now pass ID to GUI,password-assuming default one or whatever has set it to
             DocProperties* teamviewerInfo = new DocProperties();
             teamviewerInfo->Add(TV_ID_PARAM, new String(idStr));
             teamviewerInfo->Add(TV_PWD_PARAM, new String(password));

            ConfigMngr::GetInstance()->NotifyTask("GuiManager", new Message(MSG_COMMAND, CWM_RM_GETINFO, teamviewerInfo));
            // ConfigMngr::GetInstance()->NotifyTask("GuiManager", new Message(MSG_COMMAND, CWM_RM_GETINFO, new String(idStr)));

         }
     }
}

int32_t myRemoteManager::getTeamViewerStatus() {
    String statuscmd = "teamviewer daemon status";
    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
    STATUS status = RM_UNKNOWN;

    fp = popen(statuscmd.Data(), "r"); // start app and open stream to do capture data incoming from Noddy
    if(fp == NULL)
    {
        qInfo()<<"\n app doesnt exist?"<<endl;
        //shouldnot happen but if happens- notify GUI with error message
    }
    while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
    {
        if(strstr(buffer,ACTIVE_STATUS_TAG))
        {
            if(strstr(buffer,STATUS_VALUE_ACTIVE))
            {
                status = RM_ACTIVE;
                break;
            }
            else if(strstr(buffer,STATUS_VALUE_ACTIVE))
            {
                status = RM_INACTIVE;
                break;
            }
            else
            {
                status = RM_UNKNOWN;
                break;
            }
        }
     }
    if(fp) pclose(fp);
    qInfo()<<"teamviewer app status"<<status<<endl;
    setStatus(status);
}
void myRemoteManager::enableTeamViewer(int val)
{
    //This method enables/disables teamviewer to be started at Startup,Its best not to enable it as it takes up mcuh of processing power.But its required to be enabled before starting teamviewer,so stop after ending tv
    if(val > 1 || val < 0)
    {
        qInfo()<<"Invalid command"<<endl;

    }
    String cmds[2] = {"teamviewer daemon disable","teamviewer daemon enable"};
    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
    fp = popen(cmds[val].Data(), "r");
    if(fp == NULL)
    {
        qInfo()<<"\n app doesnt exist?"<<endl;
        //shouldnot happen but if happens- notify GUI with error message
    }
    while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
    {
        if(strstr(buffer,STOPPED_OP))
        {
         qInfo()<<"stopped teamviewer app"<<endl;
         setStatus(RM_STOPPED);
        }
     }
     if(fp) pclose(fp);

}
void myRemoteManager::stopTeamViewer() {

    if(getStatus() == RM_STOPPED)
    {
        //check status once again
        getTeamViewerStatus();
        qInfo()<<"APP STOPPED already"<<endl;
        return;
    }

    String stopcmd = "teamviewer daemon stop";
    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
    fp = popen(stopcmd.Data(), "r");
    if(fp == NULL)
    {
        qInfo()<<"\n app doesnt exist?"<<endl;
        //shouldnot happen but if happens- notify GUI with error message
    }
    while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
    {
        if(strstr(buffer,STOPPED_OP))
        {
         qInfo()<<"stopped teamviewer app"<<endl;
         setStatus(RM_STOPPED);
        }
     }
     if(fp) pclose(fp);

     enableTeamViewer(0);
}
void myRemoteManager::startTeamViewer() {
    if(getStatus() == RM_STARTED)
    {
        //check status once again
        getTeamViewerStatus();
        qInfo()<<"APP STARTED already"<<endl;
        return;
    }
    enableTeamViewer(1);
    String startcmd = "teamviewer daemon start";
    FILE * fp = NULL;
    char buffer[CMD_OUTLINE_LEN];
    fp = popen(startcmd.Data(), "r");
    if(fp == NULL)
    {
        qInfo()<<"\n app doesnt exist?"<<endl;
        //shouldnot happen but if happens- notify GUI with error message
    }
    while(fgets(buffer,CMD_OUTLINE_LEN,fp) != NULL)
    {
        if(strstr(buffer,STARTED_OP))
        {
         qInfo()<<"started teamviewer app"<<endl;
         setStatus(RM_STARTING);
        qInfo()<<"started teamviewer app- status set"<<endl;
        break;
        }
     }
             qInfo()<<"out of teamviewer app"<<endl;
     if(fp) pclose(fp);
     //this makes sure password is set to default one - "comet123"
     if(getStatus() == RM_STARTING)
     {
         getTeamViewerInfo();
         if(getStatus() == RM_STARTED)
         {
              qInfo()<<"teamviewer started successfully"<<endl;
         }
     }
}
//int32_t myRemoteManager::onWifiConnected(void* val) {
void* myRemoteManager::_onWifiConnected(void* sender, void *args) {
    qInfo() << "Wifi connected now \n";
    myRemoteManager* mnager = myRemoteManager::GetInstance();
    mnager->startTeamViewer();
    return 0;
}
void* myRemoteManager::_onWifiStart(void* sender, void *args) {
    myRemoteManager* rm = (myRemoteManager*) sender;
    ConfigMngr* cfgManager = ConfigMngr::GetInstance();
    qInfo() << "Wifi started from Remote manager....\n";
    // ask WifiManager if it is connected to network
    WifiManager* wifiMngr = (WifiManager*) cfgManager->GetTaskInstance("WifiManager");
    if (!wifiMngr->isConnected())
    {
        qInfo()<<"WIFI not yet connected-notify wm"<<endl;
        //wifiMngr->OnChange = myRemoteManager::_onWifiConnected;	// do daisy chain notification event
       // rm->_taskSleepTime = 5000; // timeout set to 5 seconds
       // wifiMngr->Notify((new Message(MSG_NOTIFY, NWM_WIFI_TO_NETWORK,NULL,onWifiConnected)));
        // wifiMngr->Notify((new Message(MSG_NOTIFY, NWM_WIFI_TO_NETWORK)));
//         myRemoteManager::GetInstance()->
             _rmInstance->startTeamViewer();
        // inform gui and request wifi manager to connect to PREFFEREDNETWORK/SELECTEDNETWORK, if MANUALCHOOSE inform gui & wait for action (cancel, conect_to)
        // if cancel then exit
        // if connect_to request to wifi to connec_to & wait for replay from wifi manager (conected, error, cancel)
        // if canecl exit
        // if error do RETRYCOUNT--; RETRYCOUNT==0 inform gui & exit else do retray_connect for RETRYCONNECTIONCOUNT with WAITFOR goto step 6.
    }
    else
     {
        // wifi is properly connected, go with initialization for task
         qInfo()<<"WIFI is up,teamviewer start now"<<endl;
        // we will wake up _action thread when anything will be received from thread notificator
        _rmInstance->startTeamViewer();
     }
    return NULL;
}
//void* myRemoteManager::_onWifiConnected(void* sender, void *args) {

void* myRemoteManager::_onWifiStop(void* sender, void *args) {
    qInfo() << "Wifi Stopped - stop teamviewer if its already started \n";
    _rmInstance->stopTeamViewer();
    return NULL;
}
void myRemoteManager::_RemoteNotificator() {
    qInfo()<<"In notificator"<<endl;
    //ConfigMngr* cfgManager = ConfigMngr::GetInstance();
    bool autoRun = wifi_autoRun;
    _rmInstance->startTeamViewer();
    /*if (!cfgManager->IsTaskRunning("WifiManager"))
    {
        qInfo()<<"in RM notificator - NOWIFI  : "<<endl;
        //wait for wifi to come up or throw an error
        qInfo()<<"autorun value : "<<autoRun<<endl;
        if (!autoRun)
        {
            qInfo()<<"autorun is false"<<autoRun<<endl;
            cfgManager->NotifyTask("GuiManager", new Message(ERROR_NO_WIFI));
           // return 	new Message(ERROR_NO_WIFI);		// do not start thread...!!!
        }
        else
        {
            qInfo()<<"starting wifi now"<<endl;
            // we request to start wifiManager here
            startup_t* cfgWifi = new startup_t();
            cfgWifi->onStartFunc = myRemoteManager::_onWifiStart;
            //cfgWifi->onChangeFunc = this->OnChange;
            cfgWifi->onStopFunc = myRemoteManager::_onWifiStop;
            //this->_taskSleepTime = 2000; // timeout set to 2 seconds
            uint64_t wifi_tid = cfgManager->StartTask("WifiManager", cfgWifi);
            qInfo()<<"wifi task id"<<wifi_tid<<endl;
        }
    }
    else
    {
        qInfo()<<"wifimanager already started"<<endl;

        // ask WifiManager if it is connected to network
        WifiManager* wifiMngr = (WifiManager*) cfgManager->GetTaskInstance("WifiManager");
        if (!wifiMngr->isConnected())
        {
           // wifiMngr->OnChange = myRemoteManager::_onWifiConnected;	// do daisy chain notification event
            //_taskSleepTime = 5000; // timeout set to 5 seconds
            wifiMngr->Notify((new Message(MSG_NOTIFY, NWM_WIFI_TO_NETWORK)));
            // inform gui and request wifi manager to connect to PREFFEREDNETWORK/SELECTEDNETWORK, if MANUALCHOOSE inform gui & wait for action (cancel, conect_to)
            // if cancel then exit
            // if connect_to request to wifi to connec_to & wait for replay from wifi manager (conected, error, cancel)
            // if canecl exit
            // if error do RETRYCOUNT--; RETRYCOUNT==0 inform gui & exit else do retray_connect for RETRYCONNECTIONCOUNT with WAITFOR goto step 6.
        }
        else
         {
            // wifi is properly connected, go with initialization for task
             qInfo()<<"WIFI is up,start teamviewer now"<<endl;
            // we will wake up _action thread when anything will be received from thread notificator
            _rmInstance->startTeamViewer();
         }
     }*/
}

int32_t myRemoteManager::getValue_cb(void *value) {
    qInfo() << "In getValue_cb"<<endl;
    DocProperties* wifiproperties = NULL;
    if (value) {
        Response* respond = static_cast<Response*>(value);
        if (respond)
        {
            Object* payload = static_cast<Object *> (respond->Payload());
            if (payload!=NULL)
            {
                switch(payload->Type())
                {
                case TYPE_DOCPROPERTIE:
                    wifiproperties = static_cast<DocProperties*>(payload);
                    qInfo() <<"Property values..."<<wifiproperties->ToString().c_str()<<endl;
                    wifiproperties = static_cast<DocProperties*>(payload);
                    if(wifiproperties->Has("/AutoStart"))
                    {
                        wifi_autoRun = wifiproperties->GetProperty("/AutoStart")->ToBool();
                        qInfo()<<"in CB- autostart "<< wifi_autoRun<<endl;
                    }
                    break;
                default:
                    //expecting DOCPROPERTIE here
                    qInfo() <<"Pure string value..." << static_cast<String *>(payload)->Data()<<"\n";
                    break;
                }
            }

            delete respond;
            respond = NULL;
        }
        else
        {
            qInfo()<< "No properties found :\n";
        }
    }
    return EXEC_OK;
}
Message* myRemoteManager::_start() {
    int32_t error = NOERROR;
    int valueCount = 2;	// default value for looping

    DocProperties* prop = NULL;
    int autoRun = -1;
    qInfo()<<"in RM Start  : "<<endl;
    //get wifi properties about Autostart
    ConfigMngr* cfgManager = NULL;
    if (_cfgThread !=NULL)
    {
        cfgManager = (ConfigMngr*) _cfgThread->parent;
        if (_cfgThread->arguments) {
            qInfo()<<"has args"<<endl;
            prop = static_cast<DocProperties*> (_cfgThread->arguments);
        }
        qInfo()<<"in RM -count : "<<valueCount<<endl;
        cfgManager->Command(CMD_GET_PROPERTY_VALUE, getValue_cb,new String("/DefaultConfigs/SystemDefaults/Managers/WifiManager"));
        autoRun = wifi_autoRun;
    }
    else
    {
        cfgManager = ConfigMngr::GetInstance();
        qInfo()<<"getting property now"<<endl;
        ///TODO check with MR which way to be used
        prop =  (DocProperties*) cfgManager->ConfigItems()->GetProperty("/DefaultConfigs/SystemDefaults/Managers/WifiManager");

        if(cfgManager->Working())
        {
            qInfo()<<"CM working"<<endl;
            //	cfgManager->Command(CMD_GET_PROPERTY_VALUE, getValue_cb, new String("/DefaultConfigs/SystemDefaults/Managers/WifiManager"));
        }
    }
    if(prop)
    {
        qInfo()<<"prop not null"<<endl;
        if(prop->Has("/AutoStart"))
        {
            qInfo()<<"has autostart "<<endl;
            autoRun = prop->GetProperty("/AutoStart")->ToBool();
            qInfo()<< "autoRun actually is" << autoRun <<endl;
        }
    }
    //qInfo()<<"root for CM:"<< cfgManager->HomeDir().GetString()<<endl;
    qInfo()<<"in RM -start : "<<autoRun<<endl;
    while(autoRun < 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
        qInfo()<<"in RM -starting processloop : "<<autoRun<<endl;
    this->_working = true;
    _workerLowLevel = new std::thread(&myRemoteManager::_RemoteNotificator);// we attach handler to intercept incoming signals...

    if (OnStart)
    {
            Message* startMessage = new Message(MSG_START);
            // TODO returning status to caller/higher level
            OnStart(this, startMessage);
    }
    return new Message(MSG_START);	// we should return at least rx buffer as workingMessage
}

int32_t myRemoteManager::_action(Message* msg) {
    int32_t error = NOERROR;
    qInfo()<<"in action"<<endl;
    if (error==NOERROR && OnChange!=NULL) {
        // TODO determine what to send....
        OnChange(this, new Message(msg));	// we are sending copy of working message to higher level
        // !!! DO NOT delete message here, it should be done in _stop method
    }
    return error;
}

bool myRemoteManager::_notify(Message* msg) {
    qInfo()<<"in notify"<<endl;
    if (msg) {
        Object* payload = static_cast<Object*>(msg->LParam());
        String passwordString;
        qInfo() << "RM::Notify Message: " << msg->ID() << " with value: " << msg->WParam() << " params: " << msg->LParam();
        switch (msg->ID()) {
        case MSG_COMMAND:	// commands... see Messages.h
            //any Rx from other tasks can be done through MSG_COMMAND.MSG_COMMAND to ensure correct event logging will be triggered upon starting/stoping
                switch (msg->WParam())
                {
                    case CWM_RM_START:
                        startTeamViewer();
                        break;
                    case CWM_RM_STOP:
                        stopTeamViewer();
                        break;
                    case CWM_RM_CANCEL:
                        stopTeamViewer();
                        break;
                    case CWM_RM_CHANGE_PWD:
                            if(payload)
                            {
                                if(payload->Type() == TYPE_STRING)
                                {
                                    passwordString = static_cast<String>(payload);
                                }
                            }
                            if(setPassword(passwordString))
                            {
                                qInfo()<<"Problem in setting password"<<endl;
                            }
                            break;
                   case CWM_RM_GETINFO:
                         getTeamViewerInfo();
                         break;
                   default:
                         break;
                }
            break;
        case MSG_NOTIFY:	// notifications
        default:
            break;
        }
    }
    return Task::_notify(msg);
}

// Manager specific
myRemoteManager* myRemoteManager::GetInstance() {
    qInfo()<<"in getinstance"<<endl;
    if (myRemoteManager::_rmInstance==NULL) {
        qInfo()<<"In here??"<<endl;
        myRemoteManager::_rmInstance = new myRemoteManager();
    }
    return myRemoteManager::_rmInstance;
}

